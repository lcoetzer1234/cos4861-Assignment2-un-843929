//
//  32180438_assign02_q3-1-a.cpp
//  Prj-COS4861-A2-Q3-1-a
//
//  Created by FA::B5:FF::H6 on 2018/07/28.
//  Copyright © 2018 FA::B5:FF::H6. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>
#include <regex>
#include <iomanip>
#include <sstream>

using namespace std;

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_training_file(ifstream& fileName, string& theFileName){
    string trainingFileName;
    cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
    
    theFileName = trainingFileName;
}//end void get_training_file(ifstream& fileName)

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_output_file(ofstream& fileName){
    string outputFileName;
    cout << "Enter the file name and path for the output file : " << endl; // ask user to output file name
    cin >> outputFileName; //inputs user input into fileName
    
    // Example of hard coding the location of the traing data file are:
    // fout.open("/Users/Shared/32180438_output.txt");
    fileName.open(outputFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << outputFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to output file name
        cin >> outputFileName; //inputs user input into fileName
        fileName.open(outputFileName.c_str());
    }//end while
}//end void get_output_file(ofstream& fileName)


// function to convert a to a lower case string
void toLower(basic_string<char>& s) {
    for (basic_string<char>::iterator p = s.begin();
         p != s.end(); ++p) {
        *p = tolower(*p);
    }
} //end void toLower(basic_string<char>& s)

// function to load the training data into map data structures for unigram and bigram
// input training data from a file
// output map data structures populated with training data
void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin){
    string word = "", prevword;
    map<pair<string, string>, float> temp_map;
    // regular expression to find words in the training file
    regex regex_word("\\w+");
    
    while (fin){
        fin >> word;
        
        if (regex_match(word,regex_word))
            prevword = word;
        
        fin >> word;
        if (regex_match(word,regex_word)){
            
            unigram[word]++;
            
            bigram[pair<string,string>(prevword,word)]++;
            
        }//end if
    }//end while
    
}//end void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin)

//function to genereate unigram statistics
//input: map data structure that holds data about the training set ( the corpus)
//output: unigram statistics

void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2,int& count){
    
    //define iterator variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    
    // count all the words in the unigram_map
    //  use the count variable to store the result
    count = 0;
    
    while(it != unigram_map.end()){
        count += it->second;
        it++;
    }// end while: count all the words in the map data structure
    
    //populate unigram_map2
    //unigram_map2 is a copy of unigram_map, however the data item stored as
    //a float in unigram_map is now paired with the string variable
    //this is done to prepare the statistical output for the unigrams
    
    it = unigram_map.begin();
    
    while (it != unigram_map.end()){
        unigram_map2[pair<string, float>(it->first,it->second/count)]++;
        it++;
    }// end while: populate unigram_map2
    
    
}//end void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2, ofstream& fout, int& count)

void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, int& bcount){
    
    //define iterators variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    map<pair<std::string,std::string>, float>::iterator itb = bigram_map.begin();
    
    bcount = 0;
    
    
    while(itb != bigram_map.end()){
        bcount += itb->second;
        itb++;
    }
    
    //populate bigram map2
    // bigram_map2 is a copy of bigram_map, however the data item stored as a
    // float in bigram_map is now paired with the string pair of bigram_map
    // this is done to manipulate the float value from bigram_map with the
    // data from unigram_map
    
    string word_str;
    float count_in_unigram = 0.0;
    itb = bigram_map.begin();
    while (itb != bigram_map.end()){
        it = unigram_map.begin();
        while (it != unigram_map.end()){
            if (it->first == itb->first.first)
                count_in_unigram = it->second;
            it++;
        }
        
        
        if ( count_in_unigram != 0)
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, itb->second/count_in_unigram)]++;
        else
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, 0)]++;
        itb++;
    }//end while populate bigram_map2
    
}// end void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, ofstream& fout, int& bcount)

//Function to remove the words that proceed the tags, the purpose of this function is to get a taglist from
//a user input file that condans words/tags
//Precondition textfile with words tagged
//Postcondition tags written to a data structure

void remove_words_before_tag(ifstream& fin, string& fileName, string& result){
    
    // string trainingFileName;
    //string line;
    
    //get training file
    get_training_file(fin,fileName);
    
    //regular expression remove_re is used to match the words before and including the "/"
    //character. This will be used to remove the words before and including the "/" character.
    std::regex remove_re("[\\w\\-?]+/");
    std::string content((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    string replacement = "";
    result = regex_replace(content,remove_re, replacement);
    
    fin.close();
    
} //end funtion: void remove_words_before_tag(ifstream& fin, string& fileName, string& result)


//Function to generate a list of tags
//Precondition a string exists where tagged words have been removed
//Postcondition a map data sturcture with tags and a string containing the tags ready to be used in a regular
//expression.
void generate_taglist(map<string, float>& unigram_tags,string& result){
    
    //read strings in the list, stored in var result,
    //match it with the regular expression in regex_tag
    //if it match then put it in unigram_tag
    stringstream tagStream(result);
    string pot_tag; //potential tag
    while (tagStream){
        tagStream >> pot_tag;
        unigram_tags[pot_tag]++;
    }//end while
    
    
} //end funtion: generate_taglist(map<string, float>& unigram_tags,string& result)

//Funtion to split word-tag tokens on the '/' separator
//Precondition: a user input file has been identified which contain word/tag's
//Postcondition: a string with the '/' separator removed
void tokenizer(string& fileName,string& result){
    ifstream fin;
    fin.open(fileName.c_str());
    
    
    //regular expression replace2 will be used to
    //find the "/" character in the user input and replace ist with a space.
    //the variable replacement is used for this.
    regex replace2("[//]");
    string replacement = " ";
    std::string content1((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    result = regex_replace(content1,replace2, replacement);
    
    fin.close();
    
} //end function: void tokenizer(string& fileName,string& result)

//Function to generate a data structure consisting of word and tags
//Precondition: a string containing words followed by tags and a taglist
//Postcoidtion: a stl:map data stucture cointaing words and tags
void gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 ){
    string word, prevword;
    regex regex_word("[^\\s*]+");
    //tag_list
    
    
    map<pair<string, string>, float> temp_map;
    map<string, float> unigram_map2;
    stringstream strs(result);
    while (strs){
        strs >> prevword;
        unigram_map2[prevword]++;
        strs >> word;
        unigram_map2[word]++;
        
        bigram_map[pair<string,string>(prevword,word)]++;
        
    }//end while
    
    map<pair<string,float>,int> unigram_map3;
    int bcount = 0;
    int count = 0;
    
    
    get_unigram_stats(unigram_map2, unigram_map3,count);
    get_bigram_stats(unigram_map2, bigram_map,bigram_map2, bcount);
} //end function: void gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )



void maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2,map<pair<string, string>, float>& token_est, map<pair<string,float>, int>& token_est1){
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    float max = 0.0;
    
    //declare some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1;
    map<pair<string, string>, float>::iterator it_temp;
    map<pair<string,float>, int>::iterator it_temp2;
    map<pair<string, string>, float>::iterator it_temp3 = bigram_map.begin();
    
    //declare a varible TheSearchString to identify the word which the user will give
    string TheSearchString;
    string resultStr = "";
    
    while (it_temp3 != bigram_map.end()){
        it1 = bigram_map.begin();
        it_temp2 = bigram_map2.begin();
        max = 0.0;
        TheSearchString = it_temp3->first.first;
        //ensure that the input from the user is taken to lower case
        //toLower(TheSearchString);
        
        //begin search
        //Searching for TheSearchString will begin in bigram_map, if found the word with the highgest probability
        //of following that word will be selected. These two words will be combined to find the its probability in
        //bigram_map2
        
        
        while (it1 != bigram_map.end()){
            if (it1->first.first == TheSearchString)
            {
                if ( it1->second > max){
                    max = it1->second;
                    it_temp = it1;
                }//end if
                resultStr = it1->first.first;
            }//end if
            //if (it1->first.first == "the")
            // cout << it1->first.first << " " <<it1->first.second << " " << it1->second << endl << '\n';
            
            it1++;
        }//end while
        
        if (resultStr != "")
        {
            
            string searchStr;
            searchStr = it_temp->first.first + " " + it_temp->first.second;
            
            while(it_temp2 != bigram_map2.end() ){
                if (it_temp2->first.first == searchStr){
                    
                    token_est[pair<string, string>(TheSearchString,it_temp->first.second)]++;
                    token_est1[pair<string,float>(TheSearchString +" " + it_temp->first.second,it_temp2->first.second)]++;
                    
                }//end if
                it_temp2++;
            }//end while
        }//end then part
        
        it_temp3++;
    }//end while
    
}// end maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )


//Funtion to print the most likely tag for each word
//Precondition: a data structure exist holding the words in the traiing file its most likely tag and a probability that given a certain word
// a certain tag will follow.
//Post condition: a print out on the screen listing the most likelly tag for each word in the training file with its probablility
void print_result(map<pair<string,float>, int>& token_est1){
    
    
    map<pair<string,float>, int>::iterator it_token_est1 = token_est1.begin();
    
    cout << setw(25) << "word tag" << "\t" << right << "P*(t|Wn-1)\n";
    cout << setw(25) << "---- ---" << "\t" << right << "-----------\n";
    while (it_token_est1 != token_est1.end()){
        cout << setw(25) << it_token_est1->first.first << "\t\t" << it_token_est1->first.second << endl;
        
        it_token_est1++;
    }//end while
}// end void print_result(map<pair<string,float>, int>& token_est1)

int main(int argc, const char * argv[]) {
    
    
    //1. remove 'word/'
    ifstream fin;
    string fileName, result;
    remove_words_before_tag(fin,fileName,result);
    
    
    
    //2. select tags unique ones and put in data struture
    map<string, float> unigram_tags;
    generate_taglist(unigram_tags,result);
    
    
    //3. remove the / and replace with space
    tokenizer(fileName,result);
    
    //4. produce word - tag bigrams
    map<pair<string,string>, float> bigram_map;
    map<pair<string,float>, int> bigram_map2;
    gen_word_tag_bigrams(result,bigram_map,bigram_map2);
    
    //5. compute p*(t|w) argmax p(t|w) for the words in the given corpus
    map<pair<string,string>, float> token_est;
    map<pair<string,float>, int> token_est1;
    maximum_likely_hood_estimator(bigram_map, bigram_map2,token_est, token_est1);
    
    //6. Print token estmations
    print_result(token_est1);
    
    return 0;
}






