//
//  main.cpp
//  Cos4861-Prj-assign02-q3-1-c
//
//
//
//  Created by FA::B5:FF::H6 on 2018/07/28.
//  Copyright © 2018 FA::B5:FF::H6. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>
#include <regex>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace std;

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_test_file(ifstream& fileName){
    string trainingFileName;
    cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    // Two examples of hard coding the location of the traing data file are:
    //1.) fin.open("/Users/Shared/32180438_a1q3_news2-corpora.txt");
    //2.) fin.open("/Users/Shared/32180438_a1q3_news1-corpora.txt");
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
    
}//end void get_test_file(ifstream& fileName)

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_training_file(ifstream& fileName, string& theFileName, string& prompt_msg){
    string trainingFileName;
    cout << prompt_msg << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
    
    theFileName = trainingFileName;
}//end void get_training_file(ifstream& fileName, string& theFileName)

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_output_file(ofstream& fileName){
    string outputFileName;
    cout << "Enter the file name and path for the output file : " << endl; // ask user to output file name
    cin >> outputFileName; //inputs user input into fileName
    
    // Example of hard coding the location of the traing data file are:
    // fout.open("/Users/Shared/32180438_output.txt");
    fileName.open(outputFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << outputFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to output file name
        cin >> outputFileName; //inputs user input into fileName
        fileName.open(outputFileName.c_str());
    }//end while
}//end void get_output_file(ofstream& fileName)


// function to convert a to a lower case string
void toLower(basic_string<char>& s) {
    for (basic_string<char>::iterator p = s.begin();
         p != s.end(); ++p) {
        *p = tolower(*p);
    }
} //end void toLower(basic_string<char>& s)

// function to load the training data into map data structures for unigram and bigram
// input training data from a file
// output map data structures populated with training data
void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin){
    string word = "", prevword;
    map<pair<string, string>, float> temp_map;
    // regular expression to find words in the training file
    regex regex_word("\\w+");
    
    while (fin){
        fin >> word;
        
        if (regex_match(word,regex_word))
            prevword = word;
        fin >> word;
        if (regex_match(word,regex_word)){
            unigram[word]++;
            bigram[pair<string,string>(prevword,word)]++;
        }//end if
    }//end while
    
}//end void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin)

//function to genereate unigram statistics
//input: map data structure that holds data about the training set ( the corpus)
//output: unigram statistics

void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2,int& count){
    
    //define iterator variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    
    // count all the words in the unigram_map
    //  use the count variable to store the result
    count = 0;
    
    while(it != unigram_map.end()){
        count += it->second;
        it++;
    }// end while: count all the words in the map data structure
    
    //populate unigram_map2
    //unigram_map2 is a copy of unigram_map, however the data item stored as
    //a float in unigram_map is now paired with the string variable
    //this is done to prepare the statistical output for the unigrams
    
    it = unigram_map.begin();
    
    while (it != unigram_map.end()){
        unigram_map2[pair<string, float>(it->first,it->second/count)]++;
        it++;
    }// end while: populate unigram_map2
}//end void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2, ofstream& fout, int& count)

void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, int& bcount){
    
    //define iterators variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    map<pair<std::string,std::string>, float>::iterator itb = bigram_map.begin();
    
    bcount = 0;
    
    while(itb != bigram_map.end()){
        bcount += itb->second;
        itb++;
    }
    
    //send the statistics of the unigram to the console and to an output file
    
    
    //populate bigram map2
    // bigram_map2 is a copy of bigram_map, however the data item stored as a
    // float in bigram_map is now paired with the string pair of bigram_map
    // this is done to manipulate the float value from bigram_map with the
    // data from unigram_map
    
    string word_str;
    float count_in_unigram = 0.0;
    itb = bigram_map.begin();
    while (itb != bigram_map.end()){
        it = unigram_map.begin();
        while (it != unigram_map.end()){
            if (it->first == itb->first.first)
                count_in_unigram = it->second;
            it++;
        }
        
        
        if ( count_in_unigram != 0)
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, itb->second/count_in_unigram)]++;
        else
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, 0)]++;
        itb++;
    }//end while populate bigram_map2
    
    //print bigram map2
    
    
    
    
}// end void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, ofstream& fout, int& bcount)

//Function to remove the words that proceed the tags, the purpose of this function is to get a taglist from
//a user input file that condans words/tags
//Precondition textfile with words tagged
//Postcondition tags written to a data structure

void remove_words_before_tag(ifstream& fin, string& fileName, string& result, string& prompt_msg){
    
    // string trainingFileName;
    //string line;
    
    //get training file
    get_training_file(fin,fileName,prompt_msg);
    
    //regular expression remove_re is used to match the words before and including the "/"
    //character. This will be used to remove the words before and including the "/" character.
    std::regex remove_re("[\\w\\-?:.%,;''\"0-9]+/");
    std::string content((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    string replacement = "";
    result = regex_replace(content,remove_re, replacement);
    fin.close();
    
} //end funtion: void remove_words_before_tag(ifstream& fin, string& fileName, string& result)


//Function to generate a list of tags
//Precondition a string exists where tagged words have been removed
//Postcondition a map data sturcture with tags and a string containing the tags ready to be used in a regular
//expression.
void generate_taglist(map<string, float>& unigram_tags,string& result){
    
    //read strings in the list, stored in var result,
    //match it with the regular expression in regex_tag
    //if it match then put it in unigram_tag
    stringstream tagStream(result);
    string pot_tag; //potential tag
    while (tagStream){
        tagStream >> pot_tag;
        unigram_tags[pot_tag]++;
    }//end while
    
    
} //end funtion: void generate_taglist(map<string, float>& unigram_tags,string& result)

//Funtion to split word-tag tokens on the '/' separator
//Precondition: a user input file has been identified which contain word/tag's
//Postcondition: a string with the '/' separator removed
void tokenizer(string& fileName,string& result){
    ifstream fin;
    fin.open(fileName.c_str());
    
    
    //regular expression replace2 will be used to
    //find the "/" character in the user input and replace ist with a space.
    //the variable replacement is used for this.
    regex replace2("[//]");
    string replacement = " ";
    std::string content1((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    result = regex_replace(content1,replace2, replacement);
    
    fin.close();
} //end function: void tokenizer(string& fileName,string& result)

//Function to generate a data structure consisting of word and tags
//Precondition: a string containing words followed by tags and a taglist
//Postcoidtion: a stl:map data stucture cointaing words and tags
void gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 ){
    string word, prevword;
    regex regex_word("[^\\s*]+");
    //tag_list
    
    
    map<pair<string, string>, float> temp_map;
    map<string, float> unigram_map2;
    stringstream strs(result);
    while (strs){
        strs >> prevword;
        unigram_map2[prevword]++;
        strs >> word;
        unigram_map2[word]++;
        
        bigram_map[pair<string,string>(prevword,word)]++;
        
    }//end while
    
    map<pair<string,float>,int> unigram_map3;
    int bcount = 0;
    int count = 0;
    
    
    get_unigram_stats(unigram_map2, unigram_map3,count);
    get_bigram_stats(unigram_map2, bigram_map,bigram_map2, bcount);
} //end function: void gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )



void maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2,map<pair<string, string>, float>& token_est, map<pair<string,float>, int>& token_est1){
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    float max = 0.0;
    
    //declare some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1;
    map<pair<string, string>, float>::iterator it_temp;
    map<pair<string,float>, int>::iterator it_temp2;
    map<pair<string, string>, float>::iterator it_temp3 = bigram_map.begin();
    
    
    
    //declare a varible TheSearchString to identify the word which the user will give
    string TheSearchString;
    string resultStr = "";
    
    
    while (it_temp3 != bigram_map.end()){
        it1 = bigram_map.begin();
        it_temp2 = bigram_map2.begin();
        max = 0.0;
        TheSearchString = it_temp3->first.first;
        //ensure that the input from the user is taken to lower case
        //toLower(TheSearchString);
        
        //begin search
        //Searching for TheSearchString will begin in bigram_map, if found the word with the highgest probability
        //of following that word will be selected. These two words will be combined to find the its probability in
        //bigram_map2
        
        
        while (it1 != bigram_map.end()){
            if (it1->first.first == TheSearchString)
            {
                if ( it1->second > max){
                    max = it1->second;
                    it_temp = it1;
                }//end if
                resultStr = it1->first.first;
            }//end if
            //if (it1->first.first == "the")
            // cout << it1->first.first << " " <<it1->first.second << " " << it1->second << endl << '\n';
            
            it1++;
        }//end while
        
        //Print message if search string can not be found int the given corpus/ training set
        if (resultStr != "")
        {
            string searchStr;
            searchStr = it_temp->first.first + " " + it_temp->first.second;
            
            while(it_temp2 != bigram_map2.end() ){
                if (it_temp2->first.first == searchStr){
                    
                    token_est[pair<string, string>(TheSearchString,it_temp->first.second)]++;
                    token_est1[pair<string,float>(TheSearchString +" " + it_temp->first.second,it_temp2->first.second)]++;
                    
                }//end if
                it_temp2++;
            }//end while
        }//end then part
        it_temp3++;
    }//end while
    
}// end maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )

//Function to print the result of the most likely tag
//Pre-condition: a data structre holding the word its tag and its probabilty exits
//Post-condtion: printed list of a words and their relevant tags and probabilities
void print_result(map<pair<string,float>, int>& token_est1){
    
    map<pair<string,float>, int>::iterator it_token_est1 = token_est1.begin();
    
    cout << setw(25) << "word tag" << "\t" << right << "P*(t|Wn-1)\n";
    cout << setw(25) << "---- ---" << "\t" << right << "-----------\n";
    while (it_token_est1 != token_est1.end()){
        cout << setw(25) << it_token_est1->first.first << "\t\t" << it_token_est1->first.second << endl;
        
        it_token_est1++;
    }//end while
}// end void print_result(map<pair<string,float>, int>& token_est1)

//Funtion taging produces a test file that is tagged
//Precondition: User input file with word that are not tagged, a model of word/tag and P*(tag|word) which
//condains the most probable tag for each word. This is P*(tag|word) is obtained from a training file.
//Postcondition: an annotated file containing POS tags for each word.

void pos_taging(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, string& annotated ){
    
    //get the input file from the user that contains words that does not have POS tags
    ifstream fin;
    
    get_test_file(fin);
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    float max;
    
    //declae some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1;
    map<pair<string, string>, float>::iterator it_temp;
    
    //decalre a varible TheSearchString to identify the word which the user will give
    string word;
    string tagStr = "";
    
    //regex regex_non_word("[^a-zA-z]+");
    //string replacement = "";
    
    
    //get the first word in the test file
    //The first while: while (!fin.eof()) loops through the file
    //The second while: while (it1 != bigram_map.end()) loops through the bigram_map containing the trained word/tag
    fin >> word;
    
    
    
    
    while (!fin.eof()){
        max = 0.0;
        it1 = bigram_map.begin();
        tagStr = "";
        //word = regex_replace(word,regex_non_word, replacement);
        
        //begin search
        //Searching for the word will begin in bigram_map, if found the word with the highgest probability
        //of following that word will be selected. These two words will be combined to find the its probability in
        //bigram_map2
        
        while (it1 != bigram_map.end()){
            //regex re(word);
            //if (regex_match(it1->first.first,re))
            if (it1->first.first == word){ //the word is found in the training corpus
                
                if ( it1->second > max){
                    tagStr = it1->first.second;
                    max = it1->second;
                }//end if
            }
            it1++;
        }//end while
        
        if (tagStr == "")  // the word tag was not found.
            tagStr = "nn";   //assign the tag of unknown words as 'nn'
        
        //Populate the string annotated with the word/tag combination as calculated from the while (it1 != bigram_map.end()) statement
        
        annotated += word + "/" + tagStr + " ";
        
        //get the next word in the test file
        fin >> word;
    }//end while
    
    fin.close();
    
    
}// end maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )

//Function to print the confusion matrix
//Precondition: a data structure exist in which the golden stand tags are compared with the hypothesised tags
//Postcondition: a matrix printed to the screen showing the golden standard tags where the row labels indicate the tags
// from the golden standard and the column labels indicate the tags from the hypothesised tags, the cell (x,y) contains the number of times an item with the correct classification x was classified by the model as y

void print_confusion_matrix(map<pair<string,string>,float>& conf_matr){
    
    
    //get bigram map stats -to express in terms of percentage the errors that was made
    float count_errors = 0.0;
    map<pair<string, string>, float>::iterator it_cf_matr = conf_matr.begin();
    
    //count all the errors in the confuxion matrix
    while (it_cf_matr != conf_matr.end()){
        
        count_errors += it_cf_matr->second;
        it_cf_matr++;
    }//end while
    
    
    //store the results of the confusion matrix with the errors expressed as a percentage in a data structure
    //that will be used to write the matrix to the screen
    map<pair<string,float>,int> conf_matr_output;
    
    string map_str;
    it_cf_matr = conf_matr.begin();
    while (it_cf_matr != conf_matr.end()){
        map_str = it_cf_matr->first.first + " " + it_cf_matr->first.second;
        conf_matr_output[pair<string,float>(map_str,it_cf_matr->second/count_errors*100)]++;
        it_cf_matr++;
    }//end while
    
    
    //The data structure containing the results has the following format: map<pair<string,string>,float>
    //The value stored in the first string of the pair is the golden standard tag.
    //The value stored in the second string of the pair is the hypothesised tag.
    //The value stored in the float variable is the times the hypothesised model incorrectly tagged.
    
    
    //write the values in the data structure in a matrix from which will be used for the output to the screen
    //create matrix with the values
    //count how many rows there are in the data structure
    map<pair<string,float>,int>::iterator it_conf_matr_output = conf_matr_output.begin();
    int count_rows;
    for (count_rows = 0; it_conf_matr_output != conf_matr_output.end(); it_conf_matr_output++ ){
        count_rows++;
    }//end for
    
    
    vector<vector<float>> vec(count_rows, vector<float>(count_rows));
    
    it_conf_matr_output = conf_matr_output.begin();
    for(int i = 0; i < count_rows; i++){
        for(int j = 0; j < count_rows; j++){
            if (i == j){
                vec[i][j] = it_conf_matr_output->first.second;
                it_conf_matr_output++;
            }
            else
                vec[i][j] = 0.0;
        }//end inner for loop
    }//end for
    
    
    cout << "The confusion matrix shows the diffrence between the golden standard and the hypothesised model. Golden standard tags are printed top row, hypothesised tags are printed on the left column. The cell (x,y) contains the number of times an item with correct classification x was classified by the model as y." << endl << endl;
    cout << "The confusion matrix is:\n";
    
    //write the top row of the matrix - the golden standard tags
    it_cf_matr = conf_matr.begin();
    cout << "\t\t";
    while (it_cf_matr != conf_matr.end()){
        cout << it_cf_matr->first.first << setw(5);
        it_cf_matr++;
    }//end while
    
    cout << endl;
    cout << "\t\t";
    for (int i = 0; i < count_rows; i++){
        cout << "---\t";
    }
    cout << "---" << endl;
    
    //write the columns containing the hypothesised tags and the error values
    //define a temp iterator which will be used to advance through the matrix to print out the relevant value for each item in the current row.
    //set the output only to print out 2 decimals after the "."
    
    it_cf_matr = conf_matr.begin();
    map<pair<string,string>,float>::iterator it_cf_matr_outp_tmp;
    it_conf_matr_output = conf_matr_output.begin();
    //define an integer to step through the vector
    int row;
    row = 0;
    cout << setw(5);
    while (it_conf_matr_output != conf_matr_output.end() && (row < count_rows)){
        cout << std::fixed;
        cout << std::setprecision(2);
        cout << it_cf_matr->first.second << "|" << setw(5);
        //advance through the vector structure to write the values of the current row
        //set the output only to print out 2 decimals after the "."
        for (int col = 0; col < count_rows; col++){
            //cout << std::fixed;
            //cout << std::setprecision(2);
            if (vec[row][col] <= 0.0)
                cout << " - " << setw(5);
            else
                cout << vec[row][col] << setw(5);
        }//end for
        cout << endl;
        it_cf_matr++;
        row++;
    }//end while
    
    cout << endl;
    
    
    
} //end print_confusion_matrix(map<pair<string,string>,float>& conf_matr)



//Function to compute a confusion matrix
//Precondition: a hypotesised word/tag data structure and a golden standard word/tag data structure exist
//Postcondition: a data structure that list the tag diffrences between the golden standard and the hypothesised models

void gen_confusion_matrix(map<pair<string,string>,float>& hypo_word_tags, map<pair<string,string>,float>& golden_word_tags, map<pair<string,string>,float>& conf_matr){
    
    map<pair<string, string>, float>::iterator it_hypo = hypo_word_tags.begin();
    map<pair<string, string>, float>::iterator it_gold;
    
    
    //search through the hypothesised word/tag data structure
    while (it_hypo != hypo_word_tags.end()){
        
        it_gold = golden_word_tags.begin();
        
        //search through the golden stanard word/tag data structure
        while (it_gold != golden_word_tags.end()){
            if (it_hypo->first.first == it_gold->first.first){
                if (it_hypo->first.second != it_gold->first.second)
                    conf_matr[pair<string,string>(it_gold->first.second,it_hypo->first.second)]++;
            }
            it_gold++;
        }//end while
        
        it_hypo++;
    }//end while
    
    //Print the confusion matrinx
    print_confusion_matrix(conf_matr);
    
    
} //end void gen_confusion_matrix(map<pair<string,string>,float>& hypo_word_tags, map<pair<string,string>,float>& golden_word_tags, map<pair<string,string>,float>& conf_matr)

int main(int argc, const char * argv[]) {
    
    cout << "This program computes the confusion matrix for hypothesised tags, compared with a golden standrard tags."
    << "The file name for the word/tag source for the hypothesised tags are required first, and after some computation, the file name for the golden stanard tags are required.\n\n";
    
    //Hypothesised tags
    //1.1 remove 'word/'
    ifstream fin;
    string fileName, result_hypo;
    string hypo_prompt_msg = "\nEnter the file name and path for the hypothesised word/tag data : ";
    remove_words_before_tag(fin,fileName,result_hypo,hypo_prompt_msg);
    
    
    
    //1.2 select tags and put in data struture
    map<string, float> unigram_tags_hypo;
    generate_taglist(unigram_tags_hypo,result_hypo);
    
    
    //1.3 remove the / and replace with space
    tokenizer(fileName,result_hypo);
    
    //1.4 bigram.....
    map<pair<string,string>, float> bigram_map_hypo;
    map<pair<string,float>, int> bigram_map2_hypo;
    gen_word_tag_bigrams(result_hypo,bigram_map_hypo,bigram_map2_hypo);
    
    //1.5 compute p*(t|w) argmax p(t|w) for the words in the given corpus
    map<pair<string,string>, float> token_est_hypo;
    map<pair<string,float>, int> token_est1_hypo;
    maximum_likely_hood_estimator(bigram_map_hypo, bigram_map2_hypo,token_est_hypo, token_est1_hypo);
    
    //1.6 Print token estmations
    //print_result(token_est1_hypo);
    
    
    //Standard tags
    //2.1 remove 'word/'
    ifstream fin_gs;
    string fileName_gs, result_gs;
    string gs_prompt_msg = "\nEnter the file name and path for the golden standard word/tag data : ";
    remove_words_before_tag(fin_gs,fileName_gs,result_gs,gs_prompt_msg);
    
    //2.2 select tags and put in data struture
    map<string, float> unigram_tags_gs;
    generate_taglist(unigram_tags_gs,result_gs);
    
    //2.3 remove the / and replace with space
    tokenizer(fileName_gs,result_gs);
    
    //2.4 bigram.....
    map<pair<string,string>, float> bigram_map_gs;
    map<pair<string,float>, int> bigram_map2_gs;
    gen_word_tag_bigrams(result_gs,bigram_map_gs,bigram_map2_gs);
    
    
    //2.5 compute p*(t|w) argmax p(t|w) for the words in the given corpus
    map<pair<string,string>, float> token_est_gs;
    map<pair<string,float>, int> token_est1_gs;
    maximum_likely_hood_estimator(bigram_map_gs, bigram_map2_gs,token_est_gs, token_est1_gs);
    
    //2.6 Print token estmations
    //print_result(token_est1_gs);
    
    
    
    //7. Produce an annotated output from a test set
    //string annotated ="";
    //pos_taging(bigram_map,bigram_map2, annotated);
    
    
    //Calc and print the confusion matrix
    map<pair<string,string>,float> conf_matr;
    gen_confusion_matrix(bigram_map_hypo,bigram_map_gs, conf_matr);
    
    
    return 0;
}









