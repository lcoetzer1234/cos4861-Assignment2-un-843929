README file for the file 32180438-assign02-q3-1-b.cpp

1. Project Title
COS 4861 ASSIGNMENT 2 UNIQUE NUMBER 843929 QUESTION 3-1-b
The program is contained in the file: 32180438-assign02-q3-1-b.cpp


2. Prerequisites

This program is written in C++.


3. Running the tests
The program requests a file name for the training data which will be used to generate the hypothesized tagging model. The program also requests a file name for an un annotated file, also called the test file. 

A typical output of the program at this stage is as follows:

This program produces an annotated file, that is a file where words are marked with their respective tags.
The program achieve this in 3 steps. Step 1 the program requests a training file from the user. This file is used to build up a hypothesised model to tag words. Step 2 the program request a file where words don't have any tags in a test file. Step 3 the progam requests an output file, this file contains the annotated file, that is the test file provided in step 2 will now have word/tag sequences, the tag portion will be generated from the hypothesised model. The result of the annotated file is also written to the screen.



Enter the file name and path for the training data:
/users/shared/pos_tagged.txt
Enter the file name for the test data set:
/users/shared/pos_test.txt

By using these two files the program annotates, that is produce word/tag strings and then write the result to the a file and the screen.
The output of the program at this stage is typically as follows:
*Please note that program data has been formatted for readability

Enter the file name and path for the output file : 
/users/shared/pos_test_result.txt


----The POS tagged ouput---

Time/nn stands/nn still/rb every/nn time/nn Moritz,/nn a/at 26-year-old/nn Army/nn Signal/nn Corps/nn-tl veteran/nn ,/,
goes/nn into/in the/at field./nn Although/cs he/pps never/nn gets/nn to/in play/nn while/nn the/at clock/nn is/bez running,/nn
he/pps gets/nn a/at big/nn kick/nn --/-- several/nn every/nn Saturday,/nn in/in fact/nn --/-- out/in of/in football./nn
Assistant/nn coach/nn John/np Cudmore/nn described/nn victory/nn as/cs ``a/nn good/jj feeling/nn ,/,
I/nn think/nn ,/, on/in the/at part/nn of/in the/at coaches/nn and/cc the/at players./nn
We/ppss needed/nn it/ppo and/cc we/ppss got/nn it''/nn ./. 


4. Build With
 XCODE on Mac OS




