README file for the file 32180438_assign02-q1-b.cpp

1. Project Title
COS 4861 ASSIGNMENT 2 UNIQUE NUMBER 843929 QUESTION 1b
The program is contained in the file: 32180438_assign02-q1-b.cpp


2. Prerequisites

This program is written in C++.


3. Running the tests
The program requests a file name for the training data, also known as the corups.
 a typical output of the program at this stage is as follows:

Enter the file name and path for the training data : 
/Users/Shared/32180438_a1q3_news2-corpora.txt

The program requests a word from the user, a typical output of the program at this stage is as follows:

Enter a word to search for: be


One corpora files have been tested with this program. These two files have been uploaded to GitLab.
The names of the file is:
a.) 32180438_assign02_news2_corpora.txt


By using this file, the program proceed, the program generates statistics for bigrams in memory, a sample of the data which the program generates is as follows:
*Please note that program data has been formatted for readability

Statistics for bigram

                   Bigram	Probability
                   ------	-----------
                       it			0
           10 bridesmaids	1
             14th century		1
                1662 book		1
          1947 designated	1
                  1948 as		0.5
                 1948 has		0.5
                20 gospel		0.5
                    20 or		0.5
                    50 of		1
         70th anniversary	1
                    a and		0.0217391
                a barrage		0.0217391
                a british		0.0217391
                 a church		0.0217391
          a constellation		0.0217391
                a diamond		0.0217391
                a elegant		0.0217391
                  a fiery		0.0217391
                 a future		0.0434783
                 a gilded		0.0217391
         a groundbreaking	0.0217391
                  a group		0.0217391
               a haunting		0.0217391
                   a kiss		0.0217391
                  a large		0.0217391
                 a marked		0.0217391
               a mythical		0.0217391
                    a new		0.0217391
                     a of		0.0217391
                  a peace		0.0217391
               a peaceful		0.0217391
            a performance	0.0217391
                   a plan		0.0217391
               a powerful		0.0217391
              a pragmatic		0.0217391
            a prestigious		0.0217391
               a profound		0.0217391
            a recognition		0.0217391
                  a royal		0.0217391
                 a signal		0.0217391
                 a single		0.0217391
                a soulful		0.0434783
                a special		0.0217391
                 a spirit		0.0217391
               a straight		0.0217391
               a striking		0.0217391
            a traditional		0.0652174
         ………..


The program proceeds to work out the most probable word following the word given by the user. A typical output of the program at this stage is:

                       Wn	P(Wn|Wn-1)
                       --	----------
                    based	0.2
Program ended with exit code: 0

In this illustration Wn represents the word following the word which the user provided. The word which the user provided is denoted by Wn-1. P(Wn|Wn-1) denotes the probability of Wn given Wn-1, so in this case P(based|be) = 0.2. The most probable word following “be” will be “based”. 


4. Build With
 XCODE on Mac OS




