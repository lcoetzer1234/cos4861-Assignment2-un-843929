README file for the file 32180438_assign02_q3-1-c.cpp

1. Project Title
COS 4861 ASSIGNMENT 3 UNIQUE NUMBER 843929 QUESTION 1b
The program is contained in the file: 32180438_assign02_q3-1-c.cpp


2. Prerequisites

This program is written in C++.


3. Running the tests
The program requests a file name for the training data which will be used to generate the hypothesized tagging model. The program also requests a file name for the golden standard.
 a typical output of the program at this stage is as follows:

This program computes the confusion matrix for hypothesised tags, compared with a golden standrard tags.The file name for the word/tag source for the hypothesised tags are required first, and after some computation, the file name for the golden stanard tags are required.


Enter the file name and path for the hypothesised word/tag data : 
/users/shared/pos_tagged.txt

Enter the file name and path for the golden standard word/tag data : 
/users/shared/pos_golden_standard.txt

By using these two files the program computes the confusion matrix. The output of the program at this stage is typically as follows:
*Please note that program data has been formatted for readability

The confusion matrix shows the diffrence between the golden standard and the hypothesised model. Golden standard tags are printed top row, hypothesised tags are printed on the left column. The cell (x,y) contains the number of times an item with correct classification x was classified by the model as y.

The confusion matrix is:
		,   cs   in   in   in  ppo   to
   		---	---	---	---	---	---	---	---
 ,-hl|11.11   -    -    -    -    -    - 
   ql|   - 11.11   -    -    -    -    - 
in-hl|   -    - 11.11   -    -    -    - 
in-tl|   -    -    - 11.11   -    -    - 
   rp|   -    -    -    - 33.33   -    - 
  pps|   -    -    -    -    - 11.11   - 
   in|   -    -    -    -    -    - 11.11





4. Build With
 XCODE on Mac OS




