//
//  main.cpp
//  Prj-COS4861-A2-Q1b
//
//  Created by FA::B5:FF::H6 on 2018/06/30.
//  Copyright © 2018 FA::B5:FF::H6. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <regex>
#include <iomanip>

using namespace std;

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_training_file(ifstream& fileName){
    string trainingFileName;
    cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    // Two examples of hard coding the location of the traing data file are:
    //1.) fin.open("/Users/Shared/32180438_a1q3_news2-corpora.txt");
    //2.) fin.open("/Users/Shared/32180438_a1q3_news1-corpora.txt");
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
}//end void get_training_file(ifstream& fileName)

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_output_file(ofstream& fileName){
    string outputFileName;
    cout << "Enter the file name and path for the output file : " << endl; // ask user to output file name
    cin >> outputFileName; //inputs user input into fileName
    
    // Example of hard coding the location of the traing data file are:
    // fout.open("/Users/Shared/32180438_output.txt");
    fileName.open(outputFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << outputFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to output file name
        cin >> outputFileName; //inputs user input into fileName
        fileName.open(outputFileName.c_str());
    }//end while
}//end void get_output_file(ofstream& fileName)


// function to convert a to a lower case string
// This is nescesarry because if it is not done then a word like "the" and "The" will be classified
// diffrently
void toLower(basic_string<char>& s) {
    for (basic_string<char>::iterator p = s.begin();
         p != s.end(); ++p) {
        *p = tolower(*p);
    }
} //end void toLower(basic_string<char>& s)

// function to load the training data into map data structures for unigram and bigram
// input training data from a file
// output map data structures populated with training data
void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin){
    string word = "", prevword;
    map<pair<string, string>, float> temp_map;
    // regular expression to find words in the training file
    regex regex_word("\\w+");
    
    while (fin){
        //  if (word != ""){
        //      prevword = word;
        //  }
        
        if (regex_match(word,regex_word))
            prevword = word;
        
        
        fin >> word;
        toLower(word);
        if (regex_match(word,regex_word)){
            //cout << word;
            
            unigram[word]++;
            temp_map[pair<string, string>(prevword,word)]++;
            
            float val = temp_map[pair<string,string>(prevword,word)];
            bigram[pair<string,string>(prevword,word)] = val;
        }//end if
    }//end while
    
}//end void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin)

//function to genereate unigram statistics
//input: map data structure that holds data about the training set ( the corpus)
//output: unigram statistics

void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2, ofstream& fout,int& count){
    
    //define iterator variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    
    // count all the words in the unigram_map
    //  use the count variable to store the result
    count = 0;
    
    while(it != unigram_map.end()){
        count += it->second;
        it++;
    }// end while: count all the words in the map data structure
    
    //populate unigram_map2
    //unigram_map2 is a copy of unigram_map, however the data item stored as
    //a float in unigram_map is now paired with the string variable
    //this is done to prepare the statistical output for the unigrams
    
    it = unigram_map.begin();
    
    while (it != unigram_map.end()){
        unigram_map2[pair<string, float>(it->first,it->second/count)]++;
        it++;
    }// end while: populate unigram_map2
    
    
    //send the statistics of the unigram to the console and to an output file
    cout << "\n----------------------------------------\n";
    cout << "Statistics for unigram\n";
    cout << "\n----------------------------------------\n";
    
    //write to file
    
    fout << "\n----------------------------------------\n";
    fout << "Statistics for unigram\n";
    fout << "\n----------------------------------------\n";
    
    //unigram statistics map2
    
    cout << setw(25) << "Unigram" << "\t" << right << "Probability\n";
    cout << setw(25) << "-------" << "\t" << right << "-----------\n";
    
    //write to output to file
    fout << setw(25) << "Unigram" << "\t" << right << "Probability\n";
    fout << setw(25) << "-------" << "\t" << right << "-----------\n";
    
    map<pair<string,float>,int>::iterator it2 = unigram_map2.begin();
    while (it2 != unigram_map2.end()){
        cout << setw(25) <<it2->first.first << "\t" << right <<it2->first.second << endl;
        
        //write output to file
        fout << setw(25) <<it2->first.first << "\t" << right <<it2->first.second << endl;
        
        //sum_unigram2 += it2->first.second;
        it2++;
    }//end while: map_unigram2 statistics
    
}//end void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2, ofstream& fout, int& count)


void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, int& bcount){
    
    //define iterators variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    map<pair<std::string,std::string>, float>::iterator itb = bigram_map.begin();
    
    bcount = 0;
    
    
    while(itb != bigram_map.end()){
        bcount += itb->second;
        itb++;
    }
    
    string word_str;
    float count_in_unigram = 0.0;
    itb = bigram_map.begin();
    while (itb != bigram_map.end()){
        it = unigram_map.begin();
        while (it != unigram_map.end()){
            if (it->first == itb->first.first)
                count_in_unigram = it->second;
            it++;
        }
        
        
        if ( count_in_unigram != 0)
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, itb->second/count_in_unigram)]++;
        else
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, 0)]++;
        itb++;
    }//end while populate bigram_map2
    
}// end void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, ofstream& fout, int& bcount)



//Function to implement a maximum likely hood estimator
//Pre condition a corpus is read into the program and unigram and bigrams are detemined from it
//Post condition The bigrams and corresponding statistics is then used to determine the most likely next word that will follow
// the users input word.

void maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 ){
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    float max = 0.0;
    
    //declae some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1 = bigram_map.begin();
    map<pair<string, string>, float>::iterator it_temp;
    map<pair<string,float>, int>::iterator it_temp2 = bigram_map2.begin();
    
    //decalre a varible TheSearchString to identify the word which the user will give
    string TheSearchString;
    string resultStr = "";
    
    //get user input
    cout << "\n\nEnter a word to search for: ";
    cin >> TheSearchString;
    //ensure that the input from the user is taken to lower case
    toLower(TheSearchString);
    
    //begin search
    //Searching for TheSearchString will begin in bigram_map, if found the word with the highgest probability
    //of following that word will be selected. These two words will be combined to find the its probability in
    //bigram_map2
    while (it1 != bigram_map.end()){
        if (it1->first.first == TheSearchString)
        {
            if ( it1->second > max){
                max = it1->second;
                it_temp = it1;
            }//end if
            resultStr = it1->first.first;
        }//end if
        //if (it1->first.first == "the")
        // cout << it1->first.first << " " <<it1->first.second << " " << it1->second << endl << '\n';
        
        it1++;
    }//end while
    
    //Print message if search string can not be found int the given corpus/ training set
    if (resultStr != "")
    {
        //cout << it_temp->first.first << " " << it_temp->first.second << " " << it_temp->second << endl;
        string searchStr;
        searchStr = it_temp->first.first + " " + it_temp->first.second;
        
        while(it_temp2 != bigram_map2.end() ){
            if (it_temp2->first.first == searchStr){
                cout << setw(25) << "Wn" << "\t" << right << "P(Wn|Wn-1)\n";
                cout << setw(25) << "--" << "\t" << right << "----------\n";
                cout << setw(25) << it_temp->first.second << "\t" << it_temp2->first.second << endl;
            }//end if
            it_temp2++;
        }//end while
    }//end then part
    else{
        
        cout << TheSearchString << " ,was not found in the training set," << endl;
        
    }//end else part

}// end maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )


int main(int argc, const char * argv[]) {
    
    ifstream fin;
    // string trainingFileName;
    string line;
    
    //get training file
    get_training_file(fin);
    
    //Loading traing file into unigram_map and bigram_map
    
    string word = "", prevword;
    map<string, float> unigram_map;
    map<pair<string, string>, float> bigram_map;
    map<pair<string, string>, float> temp_map;
    
    map<pair<string,float>,int> unigram_map2;
    map<pair<string,float>, int> bigram_map2;
    
    
    load_training_file_into_data_structures(unigram_map,bigram_map,fin);
    
    //close training file
    fin.close();

    int bcount;
    //get_bigram_stats(unigram_map, bigram_map,bigram_map2,fout,bcount);
    get_bigram_stats(unigram_map, bigram_map,bigram_map2,bcount);
    
    maximum_likely_hood_estimator(bigram_map,bigram_map2);
    
    return 0;
}



