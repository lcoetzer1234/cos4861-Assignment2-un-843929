//
//  main.cpp
//  Prj-Cos4861-assign2-q3-1-b
//
//
//  Created by FA::B5:FF::H6 on 2018/07/28.
//  Copyright © 2018 FA::B5:FF::H6. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <map>
#include <iterator>
#include <regex>
#include <iomanip>
#include <sstream>

using namespace std;

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_test_file(ifstream& fileName, string& screen_str){
    string trainingFileName;
    cout << screen_str << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
    
}//end void get_test_file(ifstream& fileName)





//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_training_file(ifstream& fileName, string& theFileName, string& screen_str){
    string trainingFileName;
    cout << screen_str << endl; // ask user to input file name
    cin >> trainingFileName; //inputs user input into fileName
    
    // Two examples of hard coding the location of the traing data file are:
    //1.) fin.open("/Users/Shared/32180438_a1q3_news2-corpora.txt");
    //2.) fin.open("/Users/Shared/32180438_a1q3_news1-corpora.txt");
    fileName.open(trainingFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << trainingFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to input file name
        cin >> trainingFileName; //inputs user input into fileName
        fileName.open(trainingFileName.c_str());
    }//end while
    
    theFileName = trainingFileName;
}//end void get_training_file(ifstream& fileName, string& theFileName)

//function to get the training file from the user
//input file name from user
//output training file connected to file input stream

void get_output_file(ofstream& fileName){
    string outputFileName;
    cout << "Enter the file name and path for the output file : " << endl; // ask user to output file name
    cin >> outputFileName; //inputs user input into fileName
    
    // Example of hard coding the location of the traing data file are:
    // fout.open("/Users/Shared/32180438_output.txt");
    fileName.open(outputFileName.c_str());
    
    //check if file is opned
    while (!fileName.is_open()){
        cout << "Error opening file : " << outputFileName << endl;
        cout << "Enter the file name and path for the training data : " << endl; // ask user to output file name
        cin >> outputFileName; //inputs user input into fileName
        fileName.open(outputFileName.c_str());
    }//end while
}//end void get_output_file(ofstream& fileName)


// function to load the training data into map data structures for unigram and bigram
// input training data from a file
// output map data structures populated with training data
void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin){
    string word = "", prevword;
    map<pair<string, string>, float> temp_map;
    // regular expression to find words in the training file
    regex regex_word("\\w+");
    
    while (fin){
        fin >> word;
        
        
        if (regex_match(word,regex_word))
            prevword = word;
        
        
        fin >> word;
        
        if (regex_match(word,regex_word)){
            
            
            unigram[word]++;
            
            bigram[pair<string,string>(prevword,word)]++;
            
        }//end if
    }//end while
    
}//end void load_training_file_into_data_structures(map<string,float>& unigram, map<pair<string,string>,float>& bigram, ifstream& fin)

//function to genereate unigram statistics
//input: map data structure that holds data about the training set ( the corpus)
//output: unigram statistics

void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2,int& count){
    
    //define iterator variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    
    // count all the words in the unigram_map
    //  use the count variable to store the result
    count = 0;
    
    while(it != unigram_map.end()){
        count += it->second;
        it++;
    }// end while: count all the words in the map data structure
    
    //populate unigram_map2
    //unigram_map2 is a copy of unigram_map, however the data item stored as
    //a float in unigram_map is now paired with the string variable
    //this is done to prepare the statistical output for the unigrams
    
    it = unigram_map.begin();
    
    while (it != unigram_map.end()){
        unigram_map2[pair<string, float>(it->first,it->second/count)]++;
        it++;
    }// end while: populate unigram_map2
    
    
}//end void get_unigram_stats(map<string,float>& unigram_map, map<pair<string,float>,int>& unigram_map2, ofstream& fout, int& count)

void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, int& bcount){
    
    //define iterators variable for unigram map data structures to traverse the map data structure
    map<std::string, float>::iterator it = unigram_map.begin();
    map<pair<std::string,std::string>, float>::iterator itb = bigram_map.begin();
    
    bcount = 0;
    
    
    while(itb != bigram_map.end()){
        bcount += itb->second;
        itb++;
    }
    
    //populate bigram map2
    // bigram_map2 is a copy of bigram_map, however the data item stored as a
    // float in bigram_map is now paired with the string pair of bigram_map
    // this is done to manipulate the float value from bigram_map with the
    // data from unigram_map
    
    string word_str;
    float count_in_unigram = 0.0;
    itb = bigram_map.begin();
    while (itb != bigram_map.end()){
        it = unigram_map.begin();
        while (it != unigram_map.end()){
            if (it->first == itb->first.first)
                count_in_unigram = it->second;
            it++;
        }
        
        
        if ( count_in_unigram != 0)
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, itb->second/count_in_unigram)]++;
        else
            bigram_map2[pair<string,float>(itb->first.first + " " + itb->first.second, 0)]++;
        itb++;
    }//end while populate bigram_map2
    
}// end void get_bigram_stats( map<string,float>& unigram_map, map<pair< string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, ofstream& fout, int& bcount)

//Function to remove the words that proceed the tags, the purpose of this function is to get a taglist from
//a user input file that condans words/tags
//Precondition textfile with words tagged
//Postcondition tags written to a data structure

void remove_words_before_tag(ifstream& fin, string& fileName, string& result){
    
    // string trainingFileName;
    //string line;
    
    //get training file
    string screen_str = "Enter the file name and path for the training data:";
    get_training_file(fin,fileName, screen_str);
    
    //regular expression remove_re is used to match the words before and including the "/"
    //character. This will be used to remove the words before and including the "/" character.
    std::regex remove_re("[\\w\\-?:.%,;''\"0-9]+/");
    std::string content((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    
    string replacement = "";
    
    
    result = regex_replace(content,remove_re, replacement);
    
    
    // cout << "\n\nThe result...... \n\n" << result << endl;
    
    fin.close();
    
    
} //end funtion: void remove_words_before_tag(ifstream& fin, string& fileName, string& result)


//Function to generate a list of tags
//Precondition a string exists where tagged words have been removed
//Postcondition a map data sturcture with tags and a string containing the tags ready to be used in a regular
//expression.
void generate_taglist(map<string, float>& unigram_tags,string& result){
    
    //read strings in the list, stored in var result,
    //match it with the regular expression in regex_tag
    //if it match then put it in unigram_tag
    stringstream tagStream(result);
    string pot_tag; //potential tag
    while (tagStream){
        tagStream >> pot_tag;
        unigram_tags[pot_tag]++;
    }//end while
    
    
} //end funtion: generate_taglist(map<string, float>& unigram_tags,string& result)

//Funtion to split word-tag tokens on the '/' separator
//Precondition: a user input file has been identified which contain word/tag's
//Postcondition: a string with the '/' separator removed
void tokenizer(string& fileName,string& result){
    ifstream fin;
    fin.open(fileName.c_str());
    
    
    //regular expression replace2 will be used to
    //find the "/" character in the user input and replace ist with a space.
    //the variable replacement is used for this.
    regex replace2("[//]");
    string replacement = " ";
    std::string content1((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
    
    result = regex_replace(content1,replace2, replacement);
    
    fin.close();
    
} //end function: void tokenizer(string& fileName,string& result)

//Function to generate a data structure consisting of word and tags
//Precondition: a string containing words followed by tags and a taglist
//Postcoidtion: a stl:map data stucture cointaing words and tags
void gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 ){
    string word, prevword;
    regex regex_word("[^\\s*]+");
    //tag_list
    
    
    map<pair<string, string>, float> temp_map;
    map<string, float> unigram_map2;
    stringstream strs(result);
    while (strs){
        strs >> prevword;
        unigram_map2[prevword]++;
        strs >> word;
        unigram_map2[word]++;
        
        bigram_map[pair<string,string>(prevword,word)]++;
        
    }//end while
    
    map<pair<string,float>,int> unigram_map3;
    int bcount = 0;
    int count = 0;
    
    
    get_unigram_stats(unigram_map2, unigram_map3,count);
    get_bigram_stats(unigram_map2, bigram_map,bigram_map2, bcount);
} //end function: gen_word_tag_bigrams(string& result, map<pair<string,string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )


void maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2,map<pair<string, string>, float>& token_est, map<pair<string,float>, int>& token_est1){
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    float max = 0.0;
    
    //declare some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1;
    map<pair<string, string>, float>::iterator it_temp;
    map<pair<string,float>, int>::iterator it_temp2;
    map<pair<string, string>, float>::iterator it_temp3 = bigram_map.begin();
    
    
    string TheSearchString;
    string resultStr = "";
    
    
    
    while (it_temp3 != bigram_map.end()){
        it1 = bigram_map.begin();
        it_temp2 = bigram_map2.begin();
        max = 0.0;
        TheSearchString = it_temp3->first.first;
        
        //begin search
        //Searching for TheSearchString will begin in bigram_map, if found the word with the highgest probability
        //of following that word will be selected. These two words will be combined to find the its probability in
        //bigram_map2
        
        
        while (it1 != bigram_map.end()){
            if (it1->first.first == TheSearchString)
            {
                if ( it1->second > max){
                    max = it1->second;
                    it_temp = it1;
                }//end if
                resultStr = it1->first.first;
            }//end if
            //if (it1->first.first == "the")
            // cout << it1->first.first << " " <<it1->first.second << " " << it1->second << endl << '\n';
            
            it1++;
        }//end while
        
        //Print message if search string can not be found int the given corpus/ training set
        if (resultStr != "")
        {
            //cout << it_temp->first.first << "\t" << it_temp->first.second << " " << it_temp->second << endl;
            string searchStr;
            searchStr = it_temp->first.first + " " + it_temp->first.second;
            
            while(it_temp2 != bigram_map2.end() ){
                if (it_temp2->first.first == searchStr){
                    
                    token_est[pair<string, string>(TheSearchString,it_temp->first.second)]++;
                    token_est1[pair<string,float>(TheSearchString +" " + it_temp->first.second,it_temp2->first.second)]++;
                    
                }//end if
                it_temp2++;
            }//end while
        }//end then part
        
        it_temp3++;
    }//end while
    
    
}// end maximum_likely_hood_estimator(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2 )


//....void print_result(map<pair<string,float>, int>& token_est1){


//....map<pair<string,float>, int>::iterator it_token_est1 = token_est1.begin();

//....cout << setw(25) << "word tag" << "\t" << right << "P*(t|Wn-1)\n";
//....cout << setw(25) << "---- ---" << "\t" << right << "-----------\n";
//.....while (it_token_est1 != token_est1.end()){
//....cout << setw(25) << it_token_est1->first.first << "\t\t" << it_token_est1->first.second << endl;

//.... it_token_est1++;
//....}//end while
//....}

//Funtion taging produces a test file that is tagged
//Precondition: User input file with word that are not tagged, a model of word/tag and P*(tag|word) which
//condains the most probable tag for each word. This is P*(tag|word) is obtained from a training file.
//Postcondition: an annotated file containing POS tags for each word.

void pos_taging(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, string& annotated ){
    
    //get the input file from the user that contains words that does not have POS tags
    ifstream fin;
    ofstream fout;
    string screen_str = "Enter the file name for the test data set:";
    get_test_file(fin,screen_str);
    
    //the variable max will be used to determine the word with the maximum probability following the word
    //from the user
    
    
    //The reult will be written to a file
    //get the file destination from the user
    get_output_file(fout);
    
    float max;
    
    //declare some iterator variables to search through the data stored in bigram_map and bigram_map2
    //this is primarily to find the word given, by the user, and then to find the most probable word following
    //that word
    map<pair<string, string>, float>::iterator it1;
    map<pair<string, string>, float>::iterator it_temp;
    
    //decalre a varible TheSearchString to identify the word which the user will give
    string word;
    string tagStr = "";
    
    
    //get the first word in the test file
    //The first while: while (!fin.eof()) loops through the file
    //The second while: while (it1 != bigram_map.end()) loops through the bigram_map containing the trained word/tag
    fin >> word;
    
    while (!fin.eof()){
        max = 0.0;
        it1 = bigram_map.begin();
        tagStr = "";
        
        //begin search
        //Searching for the word will begin in bigram_map, if found the word with the highgest probability
        //of following that word will be selected. These two words will be combined to find the its probability in
        //bigram_map2
        
        while (it1 != bigram_map.end()){
            //regex re(word);
            //if (regex_match(it1->first.first,re))
            if (it1->first.first == word){ //the word is found in the training corpus
                
                if ( it1->second > max){
                    tagStr = it1->first.second;
                    max = it1->second;
                }//end if
            }
            it1++;
        }//end while
        
        if (tagStr == "")  // the word tag was not found.
            tagStr = "nn";   //assign the tag of unknown words as 'nn'
        
        //Populate the string annotated with the word/tag combination as calculated from the while (it1 != bigram_map.end()) statement
        
        annotated += word + "/" + tagStr + " ";
        
        //get the next word in the test file
        fin >> word;
    }//end while
    
    //close the file that needs to be anntated
    fin.close();
    cout << "\n\n----The POS tagged ouput---\n\n";
    fout << "\n\n----The POS tagged ouput---\n\n";
    cout << annotated << endl;
    fout << annotated << endl;
    
    //close the annotated file
    fout.close();
    
}// end pos_taging(map<pair<string, string>, float>& bigram_map, map<pair<string,float>, int>& bigram_map2, string& annotated )




int main(int argc, const char * argv[]) {
    
    
    cout << "\nThis program produces an annotated file, that is a file where words are marked with their respective tags.\nThe program achieve this in 3 steps. Step 1 the program requests a training file from the user. This file is used to build up a hypothesised model to tag words. Step 2 the program request a file where words don't have any tags in a test file. Step 3 the progam requests an output file, this file contains the annotated file, that is the test file provided in step 2 will now have word/tag sequences, the tag portion will be generated from the hypothesised model. The result of the annotated file is also written to the screen.\n\n\n\n";
    
    
    //1. remove 'word/'
    ifstream fin;
    string fileName, result;
    remove_words_before_tag(fin,fileName,result);
    
    
    //2. select tags unique ones and put in data struture
    map<string, float> unigram_tags;
    generate_taglist(unigram_tags,result);
    
    
    //3. remove the / and replace with space
    tokenizer(fileName,result);
    
    //4. produce word - tag bigrams
    map<pair<string,string>, float> bigram_map;
    map<pair<string,float>, int> bigram_map2;
    gen_word_tag_bigrams(result,bigram_map,bigram_map2);
    
    //5. compute p*(t|w) argmax p(t|w) for the words in the given corpus
    map<pair<string,string>, float> token_est;
    map<pair<string,float>, int> token_est1;
    maximum_likely_hood_estimator(bigram_map, bigram_map2,token_est, token_est1);
    
    
    //6. Produce an annotated output from a test set
    string annotated ="";
    pos_taging(bigram_map,bigram_map2, annotated);
    return 0;
}







